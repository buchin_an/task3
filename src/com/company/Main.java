package com.company;

public class Main {

    public static void main(String[] args) {
        String string = "Дан текст. Нужно подсчитать количество слов и предложений в тексте. Результат вывести в консоль.";

        System.out.println("кол-во предложений: "
                + (string.length() - string.replace(".", "").length()));
        System.out.println("кол-во слов "
                + (string.length() - string.replace(" ", "").length() + 1));
    }

}
